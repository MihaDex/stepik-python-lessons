def modify_list(l):
    pos = len(l) - 1
    while pos >= 0:
        if l[pos] % 2 != 0:
            del (l[pos])
        pos = pos - 1
    for i in range(len(l)):
        l[i] = l[i] // 2
