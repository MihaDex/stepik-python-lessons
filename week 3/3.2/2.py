li = input().split()
di = {}
for i in range(len(li)):
    word = li[i].lower()
    if word in di:
        di[word] += 1
    else:
        di[word] = 1

for key in di:
    print(key, di[key])
