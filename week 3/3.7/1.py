n = int(input())
data = {}
for i in range(n):
    match = input().split(';')
    if match[0] not in data:
        if int(match[1]) > int(match[3]):
            data[match[0]] = [1, 1, 0, 0, 3]
        elif int(match[1]) == int(match[3]):
            data[match[0]] = [1, 0, 1, 0, 1]
        elif int(match[1]) < int(match[3]):
            data[match[0]] = [1, 0, 0, 1, 0]
    else:
        if int(match[1]) > int(match[3]):
            a = data[match[0]]
            a[0] += 1
            a[1] += 1
            a[4] += 3
            data[match[0]] = a
        elif int(match[1]) == int(match[3]):
            a = data[match[0]]
            a[0] += 1
            a[2] += 1
            a[4] += 1
            data[match[0]] = a
        elif int(match[1]) < int(match[3]):
            a = data[match[0]]
            a[0] += 1
            a[3] += 1
            a[4] += 0
            data[match[0]] = a

    if match[2] not in data:
        if int(match[3]) > int(match[1]):
            data[match[2]] = [1, 1, 0, 0, 3]
        elif int(match[1]) == int(match[3]):
            data[match[2]] = [1, 0, 1, 0, 1]
        elif int(match[3]) < int(match[1]):
            data[match[2]] = [1, 0, 0, 1, 0]
    else:
        if int(match[3]) > int(match[1]):
            a = data[match[2]]
            a[0] += 1
            a[1] += 1
            a[4] += 3
            data[match[2]] = a
        elif int(match[1]) == int(match[3]):
            a = data[match[2]]
            a[0] += 1
            a[2] += 1
            a[4] += 1
            data[match[2]] = a
        elif int(match[3]) < int(match[1]):
            a = data[match[2]]
            a[0] += 1
            a[3] += 1
            a[4] += 0
            data[match[2]] = a

for w in data:
    s = data[w]
    print(str(w)+':', s[0], s[1], s[2], s[3], s[4])
