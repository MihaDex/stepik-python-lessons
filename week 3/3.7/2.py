code = [list(a) for a in zip(list(input()), list(input()))]


def decode(s, mode):
    s = list(s)
    res = ''
    for i in s:
        for key in code:
            if mode:
                if i == key[0]:
                    res += key[1]
            else:
                if i == key[1]:
                    res += key[0]
    return res


sdecode = input()
sencode = input()
print(decode(sdecode, True))
print(decode(sencode, False))
