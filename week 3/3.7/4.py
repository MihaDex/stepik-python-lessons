path = [input().split() for i in range(int(input()))]
a = 0
b = 0
for j in path:
    if j[0] == "север":
        b += int(j[1])
    elif j[0] == "восток":
        a += int(j[1])
    elif j[0] == "юг":
        b -= int(j[1])
    elif j[0] == "запад":
        a -= int(j[1])

print(a, b)
