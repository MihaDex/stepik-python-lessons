inp = []
with open("class.txt", "r") as inpt:
    for line in inpt:
        inp.append(line.split())

data = {i: [0, 0] for i in range(1, 12)}
for s in inp:
    a = data[int(s[0])]
    a[0] += int(s[2])
    a[1] += 1
    data[int(s[0])] = a

with open("result.txt", "w") as out:
    for cls in data:
        if data[cls][0] != 0:
            out.write(str(cls) + " " + str(data[cls][0] / data[cls][1]) + "\n")
        else:
            out.write(str(cls) + " -\n")
