data = []
with open("input.txt", "r", encoding='utf-8') as file:
    for line in file:
        data.append(line.strip().split(';'))

mt = 0
fz = 0
ru = 0
with open("output.txt", "w") as out:
    for i in range(len(data)):
        st = str((int(data[i][1]) + int(data[i][2]) + int(data[i][3])) / 3)
        mt += int(data[i][1])
        fz += int(data[i][2])
        ru += int(data[i][3])
        out.write(st + "\n")

    out.write(str(mt / len(data)) + " " + str(fz / len(data)) + " " + str(ru / len(data)))
