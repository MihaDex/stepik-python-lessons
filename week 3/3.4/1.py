with open("input.txt") as file:
    s = file.readline()

a = 0
s1 = ""
for i in range(len(s)):
    if not s[i].isdigit() and i > 0:
        for j in range(int(s[a + 1: i])):
            s1 += s[a]
        a = i
with open("output.txt", 'w') as ouf:
    ouf.write(s1)
