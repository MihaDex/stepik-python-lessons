text = open("input.txt", 'r')
lst = text.read().replace('\n', ' ').lower().split()
text.close()

res = {x: lst.count(x) for x in lst}
print(max(res, key=res.get), res[max(res, key=res.get)])
