import requests

url = "https://stepic.org/media/attachments/course67/3.6.3/"
with open("input.txt", "r") as inp:
    s = inp.readline().strip()

addr = ""
st = requests.get(s).text
while True:
    addr = url+st
    txt = requests.get(url+st).text
    if txt.split()[0] == "We":
        break
    st = txt

print(txt)