import requests

with open("input.txt", "r") as inp:
    s = inp.readline().strip()

st = requests.get(s)

print(len(st.text.splitlines()))
