n = int(input())
if 0 <= n <= 1000:
    if 5 <= n <= 20 or 5 <= n % 100 <= 20:
        print(n, " программистов")
    elif n == 1 or n % 100 == 1 or 2 <= n // 10 <= 99 and n % 10 == 1:
        print(n, " программист")
    elif 2 <= n <= 4 or 2 <= n % 100 <= 4 or 2 <= n // 10 <= 99 and 2 <= n % 10 <= 4:
        print(n, " программиста")
    else:
        print(n, " программистов")
