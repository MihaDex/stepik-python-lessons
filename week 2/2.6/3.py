lst = [int(i) for i in input().split()]
x = int(input())
s = ""
flag = 0
for j in range(len(lst)):
    if lst[j] == x:
        s += str(j) + " "
        flag = 1

if flag:
    print(s)
else:
    print("Отсутствует")
