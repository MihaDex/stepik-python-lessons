n = int(input())
mat = [[0] * n for i in range(n)]
k = 1
x = 0
y = 0
start = 0
stop = n
path = 'right'
for i in range(n * n):

    if path == "right":
        mat[y][x] = k
        x = x + 1
        k = k + 1
        if x not in range(start, stop):
            path = "down"
            y = y + 1
            x = x - 1

    elif path == "down":
        mat[y][x] = k
        y = y + 1
        k = k + 1
        if y not in range(start, stop):
            path = "left"
            x = x - 1
            y = y - 1

    elif path == "left":
        mat[y][x] = k
        x = x - 1
        k = k + 1
        if x not in range(start, stop):
            path = "up"
            y = y - 1
            x = x + 1
            start = start + 1

    elif path == "up":
        mat[y][x] = k
        y = y - 1
        k = k + 1
        if y not in range(start, stop):
            path = "right"
            stop = stop - 1
            x = x + 1
            y = y + 1

for a in range(len(mat)):
    for b in range(len(mat[a])):
        print(mat[a][b], end=" ")
    print()
