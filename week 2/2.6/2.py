n = input()
result = ""
k = 1
a = 1
for i in range(int(n)):
    if i == 0:
        result += "1" + " "
        k += 1
    else:
        if a != k:
            result += str(k) + " "
            a += 1
        else:
            result += str(k) + " "
            a = 1
            k += 1

print(result)
