mas = []
while 1:
    name = input()
    if name == "end":
        break
    mas.append([int(i) for i in name.split()])

res = []
for a in range(len(mas)):
    res.append([0] * len(mas[a]))

sum = 0
for i in range(len(mas)):
    for j in range(len(mas[i])):
        if i == len(mas) - 1 and j == len(mas[i]) - 1:
            sum = mas[i - 1][j] + mas[0][j] + mas[i][j - 1] + mas[i][0]
        elif i == len(mas) - 1:
            sum = mas[i - 1][j] + mas[0][j] + mas[i][j - 1] + mas[i][j + 1]
        elif j == len(mas[i]) - 1:
            sum = mas[i - 1][j] + mas[i + 1][j] + mas[i][j - 1] + mas[i][0]
        else:
            sum = mas[i - 1][j] + mas[i + 1][j] + mas[i][j - 1] + mas[i][j + 1]

        res[i][j] = sum

for a in range(len(res)):
    for b in range(len(res[a])):
        print(res[a][b], end=" ")
    print()
