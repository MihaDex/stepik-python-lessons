t = True
while t:
    n = int(input())
    if n < 10:
        continue
    elif 10 <= n <= 100:
        print(n)
    elif n > 100:
        t = False
