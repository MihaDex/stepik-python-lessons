s = [int(i) for i in input().split()]
st = []
if len(s) != 1:
    s.sort()
    for j in range(len(s)):
        if j + 1 < len(s) and s[j] == s[j + 1] and st.count(s[j]) == 0:
            st.append(s[j])
print(' '.join(map(str, st)))
