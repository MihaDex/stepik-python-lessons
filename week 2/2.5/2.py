s = [int(i) for i in input().split()]
st = ""
if len(s) == 1:
    st = str(s[0])
elif len(s) == 2:
    st = st + str(s[1] + s[1]) + " "
    st += str(s[0] + s[0])
else:
    for j in range(len(s)):
        if len(s) - 1 == j:
            st = st + str(s[j - 1] + s[0]) + " "
        elif j == 0:
            st = st + str(s[-1] + s[1]) + " "
        else:
            st = st + str(s[j - 1] + s[j + 1]) + " "
print(st.rstrip(' '))
